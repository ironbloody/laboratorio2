#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class dlgAdvertencia():
	def __init__(self, titulo=""):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("/home/v/Descargas/Laboratorio2/ejemplo.ui")

		self.advertencia = self.builder.get_object("dlgAdvertencia")
		self.advertencia.set_title(titulo)
		self.advertencia.resize(600, 400)
		self.advertencia.show_all()
		
		# boton aceptar
		boton_aceptar = self.advertencia.add_button(Gtk.STOCK_OK,
                                                Gtk.ResponseType.OK)
		boton_aceptar.set_always_show_image(True)
		boton_aceptar.connect("clicked", self.boton_aceptar_clicked)

	def boton_aceptar_clicked(self, btn=None):
		self.advertencia.destroy()
